# Norwegian Bokmål translation for Clawsker.
# Copyright (C) 2015-2024 Ricardo Mones
# This file is distributed under the same license as the Clawsker package.
# Petter Adsen <petter@synth.no>, 2015, 2016, 2017, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: clawsker 1.1.0\n"
"Report-Msgid-Bugs-To: ricardo@mones.org\n"
"POT-Creation-Date: 2018-08-08 00:29+0200\n"
"PO-Revision-Date: 2018-08-14 09:50+0200\n"
"Last-Translator: Petter Adsen <petter@synth.no>\n"
"Language-Team: Bokmål, Norwegian; Norwegian Bokmål <>\n"
"Language: Norwegian Bokmål\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../clawsker:62
msgid "Claws Mail Hidden Preferences"
msgstr "Claws Mail Skjulte Innstillinger"

#: ../clawsker:63
msgid "About..."
msgstr "Om..."

#: ../clawsker:64
msgid "Clawsker :: A Claws Mail Tweaker"
msgstr "Clawsker :: Tweaker For Claws Mail"

#: ../clawsker:65
msgid "License:"
msgstr "Lisens:"

#: ../clawsker:66
msgid "Version:"
msgstr "Versjon:"

#: ../clawsker:68
msgid "Clawsker warning"
msgstr "Clawsker-advarsel"

#: ../clawsker:69
msgid "There are unapplied modifications."
msgstr "Det er ubrukte endringer."

#: ../clawsker:70
msgid "Do you really want to quit?"
msgstr "Vil du virkelig avslutte?"

#: ../clawsker:72
msgid "Colours"
msgstr "Farger"

#: ../clawsker:73
msgid "Behaviour"
msgstr "Oppførsel"

#: ../clawsker:74
msgid "GUI"
msgstr "GUI"

#: ../clawsker:75 ../clawsker:2012
msgid "Other"
msgstr "Øvrig"

#: ../clawsker:76
msgid "Windows"
msgstr "Vinduer"

#: ../clawsker:77 ../clawsker:2008
msgid "Accounts"
msgstr "Kontoer"

#: ../clawsker:78
msgid "Plugins"
msgstr "Plugins"

#: ../clawsker:79
msgid "Hotkeys"
msgstr "Hurtigtaster"

#: ../clawsker:80
msgid "Info"
msgstr "Informasjon"

#: ../clawsker:82 ../clawsker:2007
msgid "Addressbook"
msgstr "Adressebok"

#: ../clawsker:83
msgid "Memory"
msgstr "Minne"

#: ../clawsker:84 ../clawsker:93
msgid "Message View"
msgstr "Meldingsvindu"

#: ../clawsker:85 ../clawsker:1986
msgid "Log window"
msgstr "Loggvindu"

#: ../clawsker:86
msgid "Drag 'n' drop"
msgstr "Dra og slipp"

#: ../clawsker:87
msgid "Secure Sockets Layer"
msgstr "Secure Sockets Layer"

#: ../clawsker:88
msgid "Messages"
msgstr "Meldinger"

#: ../clawsker:89
msgid "Completion"
msgstr "Komplettering"

#: ../clawsker:90
msgid "Coloured stripes"
msgstr "Fargede striper"

#: ../clawsker:91
msgid "Scroll bars"
msgstr "Scroll bars"

#: ../clawsker:92
msgid "Message List"
msgstr "Meldingsliste"

#: ../clawsker:94 ../clawsker:98
msgid "Compose window"
msgstr "Komponeringsvindu"

#: ../clawsker:95
msgid "NetworkManager"
msgstr "NetworkManager"

#: ../clawsker:96
msgid "Viewing patches"
msgstr "Patch-visning"

#: ../clawsker:97
msgid "Master passphrase"
msgstr "Hovedpassord"

#: ../clawsker:99
msgid "Quick search"
msgstr "Hurtigsøk"

#: ../clawsker:101
msgid "Use detached address book edit dialogue"
msgstr "Bruk egen dialog for å editere adressebok"

#: ../clawsker:102
msgid ""
"If true use a separate dialogue to edit a person's details. Otherwise will "
"use a form embedded in the address book's main window."
msgstr ""
"Bruk egen dialog for å editere en persons detaljer. Ellers vil en dialog i "
"adressebokens hovedvindu anvendes."

#: ../clawsker:103
msgid "Maximum memory for message cache"
msgstr "Maksimalt minne for meldings-cache"

#: ../clawsker:104
msgid "kilobytes"
msgstr "kilobyte"

#: ../clawsker:105
msgid "The maximum amount of memory to use to cache messages, in kilobytes."
msgstr "Maksimalt minne brukt for å cache meldinger, i kilobyte."

#: ../clawsker:106
msgid "Minimun time for cache elements"
msgstr "Minimum tid for cachede elementer"

#: ../clawsker:107
msgid "minutes"
msgstr "minutter"

#: ../clawsker:108
msgid ""
"The minimum time in minutes to keep a cache in memory. Caches more recent "
"than this time will not be freed, even if the memory usage is too high."
msgstr ""
"Minimum tid i minutter å holde ett cache i minne. Cacher som er yngre enn "
"denne tid vil ikke frigis, selv om minnebruken er for høy."

#: ../clawsker:109
msgid "Use NetworkManager"
msgstr "Bruk NetworkManager"

#: ../clawsker:110
msgid "Use NetworkManager to switch offline automatically."
msgstr "Bruk NetworkManager for å gå offline automatisk."

#: ../clawsker:111
msgid "Rounds for PBKDF2 function"
msgstr "Antall PKBDF2-runder"

#: ../clawsker:112
msgid ""
"Specify the number of iterations the key derivation function will be applied "
"on master passphrase computation. Does not modify currently stored "
"passphrase, only master passphrases computed after changing this value are "
"affected."
msgstr ""
"Spesifiser hvor mange interasjoner funksjonen for derivering av nøkler vil "
"gjennomgå under utregning av hoved-passphrase. Endrer ikke nåværende lagrede "
"passphrase, bare hoved-passphraser som regnes ut etter endring av denne "
"verdien."

#: ../clawsker:114
msgid "Show unread messages with bold font"
msgstr "Vis uleste meldinger med fet skrifttype"

#: ../clawsker:115
msgid "Show unread messages in the Message List using a bold font."
msgstr "Vis uleste meldinger i Meldingslisten med fet skrifttype."

#: ../clawsker:116
msgid "Don't use markup"
msgstr "Ikke bruk markup"

#: ../clawsker:117
msgid "Don't use bold and italic text in Compose dialogue's account selector."
msgstr "Ikke bruk fet og kursiv text i komponerings-dialogens konto-velger."

#: ../clawsker:118
msgid "Use dotted lines in tree view components"
msgstr "Bruk stiplet linje i trevisnings-komponenter"

#: ../clawsker:119
msgid ""
"Use the old dotted line look in the main window tree views (Folder, Message "
"and other lists) instead of the modern lineless look."
msgstr ""
"Bruk gammelt utseende med stiplet linje i hovedvinduets trevisninger (Mappe, "
"Meldinger og andre lister) istedet for det moderne utseendet uten linjer."

#: ../clawsker:120
msgid "Enable horizontal scrollbar"
msgstr "Aktiver horisontal scrollbar"

#: ../clawsker:121
msgid "Enable the horizontal scrollbar in the Message List."
msgstr "Aktiver horisontal scrollbar i Meldings-listen."

#: ../clawsker:122
msgid "Display To column instead From column in Sent folder"
msgstr "Vis Til-kolonnen istedet for Fra-kolonnen i Sendt-mappen"

#: ../clawsker:123
msgid ""
"Display the recipient's email address in a To column of the Sent folder "
"instead of the originator's one in a From column."
msgstr ""
"Vis mottagerens emailadresse i en Til-kolonne i Sendt-mappen istedet for "
"avsenderens i en Fra-kolonne."

#: ../clawsker:124
msgid "Folder List scrollbar behaviour"
msgstr "Oppførsel for Meldings-listens scrollbar"

#: ../clawsker:125
msgid ""
"Specify the policy of vertical scrollbar of Folder List: show always, "
"automatic or hide always."
msgstr ""
"Spesifiser oppførsel for vertikal scrollbar i Meldings-liste: Vis alltid, "
"automatisk eller skjul alltid."

#: ../clawsker:126
msgid "Show always"
msgstr "Vis alltid"

#: ../clawsker:127
msgid "Automatic"
msgstr "Automatisk"

#: ../clawsker:128
msgid "Hide always"
msgstr "Skjul alltid"

#: ../clawsker:129
msgid "From column displays"
msgstr "Fra-kolonnen viser"

#: ../clawsker:130
msgid ""
"Selects the data displayed in the From column of the Message List: name, "
"address or both."
msgstr ""
"Velger hva som vises i Fra-kolonnen i Meldings-listen: Navn, adresse eller "
"begge."

#: ../clawsker:131
msgid "Name only"
msgstr "Kun navn"

#: ../clawsker:132
msgid "Address only"
msgstr "Kun adresse"

#: ../clawsker:133
msgid "Both name and address"
msgstr "Både navn og adresse"

#: ../clawsker:134
msgid "Coloured lines contrast"
msgstr "Kontrast for fargede linjer"

#: ../clawsker:135
msgid ""
"Specify the value to use when creating alternately coloured lines in tree "
"view components. The smaller the value, the less visible the difference in "
"the alternating colours of the lines."
msgstr ""
"Spesifiser verdien som brukes for å lage linjer med alternerende farger i "
"trevisnings-komponenter. Mindre verdi gir mindre synlig forskjell i de "
"alternerende fargene på linjene."

#: ../clawsker:136
msgid "Show cursor"
msgstr "Vis markør"

#: ../clawsker:137
msgid "Display the cursor in the Message View."
msgstr "Vis markør i Meldings-vindu."

#: ../clawsker:138
msgid "Detachable toolbars"
msgstr "Avtagbare verktøyslinjer"

#: ../clawsker:139
msgid "Show handles in the toolbars."
msgstr "Vis håndtak på verktøyslinjer."

#: ../clawsker:140
msgid "Use stripes in all tree view components"
msgstr "Bruk striper i alle trevisnings-komponenter"

#: ../clawsker:141
msgid "Enable alternately coloured lines in all tree view components."
msgstr "Bruk alternerende fargede linjer i alle trevisnings-komponenter."

#: ../clawsker:142
msgid "Use stripes in Folder List and Message List"
msgstr "Bruk striper i Mappe-liste og Meldings-liste"

#: ../clawsker:143
msgid "Enable alternately coloured lines in Message List and Folder List."
msgstr "Aktiver alternerende fargede linjer i Meldings-liste og Mappe-liste."

#: ../clawsker:144
msgid "2 lines per Message List item in 3-column layout"
msgstr "2 linjer per Meldings-listeobjekt i 3-kolonnes layout"

#: ../clawsker:145
msgid ""
"Spread Message List information over two lines when using the three column "
"mode."
msgstr ""
"Bruk to linjer for hvert objekt i Meldings-listen når 3-kolonnes layout "
"brukes."

#: ../clawsker:146
msgid "Show margin"
msgstr "Vis marg"

#: ../clawsker:147
msgid "Shows a small margin in the Compose View."
msgstr "Viser en liten marg i komponerings-dialogen."

#: ../clawsker:148
msgid "Don't display localized date"
msgstr "Ikke vis lokalisert dato"

#: ../clawsker:149
msgid "Toggles localization of date format in Message View."
msgstr "Skifter lokalisering av dato i Meldings-dialogen."

#: ../clawsker:150
msgid "Zero replacement character"
msgstr "Tegn for utskiftning av null"

#: ../clawsker:151
msgid "Replaces '0' with the given character in Folder List."
msgstr "Erstatter '0' med gitt bokstav i Mappelisten."

#: ../clawsker:152
msgid "Editable headers"
msgstr "Editerbare headers"

#: ../clawsker:153
msgid ""
"Allows to manually type any value in Compose Window header entries or just "
"select from the available choices in the associated dropdown list."
msgstr ""
"Tillat å manuelt skrive inn enhver verdi i Komponerings-vinduets headere "
"eller bare velge fra tilgjengelige valg i listen."

#: ../clawsker:154
msgid "Warn when sending to more than"
msgstr "Advar ved sending til fler enn"

#: ../clawsker:155
msgid "recipients"
msgstr "mottakere"

#: ../clawsker:156
msgid ""
"Show a warning dialogue when sending to more recipients than specified. Use "
"0 to disable this check."
msgstr ""
"Vis varseldialog ved sending til fler mottakere enn spesifisert. Bruk 0 for "
"å skru av denne sjekken."

#: ../clawsker:157
msgid "Select next message on delete"
msgstr "Velg neste melding ved sletting"

#: ../clawsker:158
msgid ""
"When deleting a message, toggles between selecting the next one (newer "
"message) or the previous one (older message)."
msgstr ""
"Velger mellom å velge neste (nyere melding) eller forrige (eldre melding) "
"når en melding slettes."

#: ../clawsker:160
msgid "Drag 'n' drop hover timeout"
msgstr "Timeout for dra og slipp-hover"

#: ../clawsker:161
msgid "milliseconds"
msgstr "millisekunder"

#: ../clawsker:162
msgid ""
"Time in milliseconds that will cause a folder tree to expand when the mouse "
"cursor is held over it during drag and drop."
msgstr ""
"Tid i millisekunder innen ett mappe-tre ekspanderer når pekeren holdes over "
"den under dra og slipp."

#: ../clawsker:163
msgid "Don't confirm deletions (dangerous!)"
msgstr "Ikke bekreft slettinger (farlig!)"

#: ../clawsker:164
msgid "Don't ask for confirmation before definitive deletion of emails."
msgstr "Ikke spør om bekreftelse ved sletting av email."

#: ../clawsker:165
msgid "Respect format=flowed in messages"
msgstr "Respekter format=flowed i meldinger"

#: ../clawsker:166
msgid ""
"Respect format=flowed on text/plain message parts. This will cause some "
"mails to have long lines, but will fix some URLs that would otherwise be "
"wrapped."
msgstr ""
"Respekter format=flowed i text/plain meldinger. Dette forårsaker at noen "
"mailer får lange linjer, men vil fikse noen URL'er som ellers ville brytes."

#: ../clawsker:167
msgid "Allow writable temporary files"
msgstr "Tillat skrivbare temporære filer"

#: ../clawsker:168
msgid "Saves temporary files when opening attachment with write bit set."
msgstr "Lagrer temporære filer når vedlegg med skrive-bit åpnes."

#: ../clawsker:169
msgid "Don't check SSL certificates"
msgstr "Ikke kontroller SSL-sertifikater"

#: ../clawsker:170
msgid "Disables the verification of SSL certificates."
msgstr "Skrur av verifisering av SSL-sertifikater."

#: ../clawsker:171
msgid "Progress bar update step every"
msgstr "Oppdater forløpsindikator hver"

#: ../clawsker:172
msgid "items"
msgstr "punkt"

#: ../clawsker:173
msgid "Update stepping in progress bars."
msgstr "Oppdateringssteg i forløpsindikatorer."

#: ../clawsker:174
msgid "Maximum age when threading by subject"
msgstr "Maksimal alder når meldinger vises i tråd etter emne"

#: ../clawsker:175
msgid "days"
msgstr "dager"

#: ../clawsker:176
msgid ""
"Number of days to include a message in a thread when using \"Thread using "
"subject in addition to standard headers\"."
msgstr ""
"Antall dager å inkludere i en tråd når \"Tråd bruker emne i tillegg til "
"standard headere\"."

#: ../clawsker:177
msgid "Allow unsafe SSL certificates"
msgstr "Tillat usikre SSL-sertifikater"

#: ../clawsker:178
msgid ""
"Allows Claws Mail to remember multiple SSL certificates for a given server/"
"port."
msgstr ""
"Tillater Claws Mail å huske flere SSL-sertifikater for en angitt server/port."

#: ../clawsker:179
msgid "Force UTF-8 for broken mails"
msgstr "Tving UTF-8 for ødelagte mailer"

#: ../clawsker:180
msgid "Use UTF-8 encoding for broken mails instead of current locale."
msgstr "Bruk UTF-8 enkoding for ødelagte mailer istedenfor aktuelt lokale."

#: ../clawsker:181
msgid "Warn on drag 'n' drop"
msgstr "Advar ved dra og slipp"

#: ../clawsker:182
msgid "Display a confirmation dialogue on drag 'n' drop of folders."
msgstr "Vis en bekreftelses-dialog ved dra og slipp av mapper."

#: ../clawsker:183
msgid "Outgoing messages fallback to ASCII"
msgstr "Utgående meldinger faller tilbake til ASCII"

#: ../clawsker:184
msgid ""
"If allowed by content, ASCII will be used to encode outgoing messages, "
"otherwise the user-defined encoding is always enforced."
msgstr ""
"Hvis innholdet tillater, vil ASCII alltid brukes til å enkode utgående "
"meldinger, ellers brukes alltid brukerdefinert enkoding."

#: ../clawsker:185
msgid "Primary paste unselects selection"
msgstr "Primær lim avmarkerer valg"

#: ../clawsker:186
msgid ""
"Controls how pasting using middle-click changes the selected text and "
"insertion point."
msgstr ""
"Kontrollerer hvordan å lime inn med midt-knappen endrer valgt tekst og "
"insettningspunkt."

#: ../clawsker:187
msgid "Show inline attachments"
msgstr "Vis inline-vedlegg"

#: ../clawsker:188
msgid "Allows to hide inline attachments already shown in mail structure view."
msgstr "Tillater å skjule inline-vedlegg som allerede vises i mail-vindu."

#: ../clawsker:189
msgid "Address search in compose window matches any"
msgstr "Adressesøk i komponeringsvindu matcher alle"

#: ../clawsker:190
msgid ""
"On Tab-key completion, address text will match any part of the string or "
"only from the start."
msgstr ""
"Ved Tab-komplettering kommer adresse-teksten til å matche enhver del av "
"strengen eller bare fra begynnelsen."

#: ../clawsker:191
msgid "Folder search in folder selector matches any"
msgstr "Søkning i mappe matcher alle"

#: ../clawsker:192
msgid ""
"On folder name completion text will match any part of the string or only "
"from the start."
msgstr ""
"Komplettering av mappe-navn vil matche enhver del av strengen eller bare fra "
"begynnelsen."

#: ../clawsker:193
msgid "Rewrite first 'From' using QP encoding"
msgstr "Skriv om første 'From' med QP enkoding"

#: ../clawsker:194
msgid ""
"Workaround some servers which convert first 'From' to '>From' by using "
"Quoted-Printable transfer encoding instead of 7bit/8bit encoding."
msgstr ""
"Løsning for noen servere som konverterer første 'From' til '>From' ved å "
"bruke Quoted-Printable enkoding i stedet for 7bit/8bit enkoding."

#: ../clawsker:196
msgid "X-Mailer header"
msgstr "X-Mailer header"

#: ../clawsker:197
msgid "The colour used for the X-Mailer line when its value is Claws Mail."
msgstr "Farge som brukes for X-Mailer linje når verdien er Claws Mail."

#: ../clawsker:198
msgid "Error messages"
msgstr "Feilmeldinger"

#: ../clawsker:199
msgid "Colour for error messages in log window."
msgstr "Farge for feilmeldinger i logg-vindu."

#: ../clawsker:200
msgid "Server messages"
msgstr "Servermeldinger"

#: ../clawsker:201
msgid "Colour for messages received from servers in log window."
msgstr "Farge for meldinger mottatt fra server i logg-vindu."

#: ../clawsker:202
msgid "Standard messages"
msgstr "Standard meldinger"

#: ../clawsker:203
msgid "Colour for messages in log window."
msgstr "Farge for meldinger i logg-vindu."

#: ../clawsker:204
msgid "Client messages"
msgstr "Klient-meldinger"

#: ../clawsker:205
msgid "Colour for messages sent to servers in log window."
msgstr "Farge for meldinger sendt til servere i logg-vindu."

#: ../clawsker:206
msgid "Warning messages"
msgstr "Varsels-meldinger"

#: ../clawsker:207
msgid "Colour for warning messages in log window."
msgstr "Farge for advarsler i logg-vindu."

#: ../clawsker:209
msgid "Tags background"
msgstr "Markerings-bakgrunn"

#: ../clawsker:210
msgid "Background colour for tags in message view."
msgstr "Bakgrunnsfarge for markeringer i meldingsvindu"

#: ../clawsker:211
msgid "Tags text"
msgstr "Markeringstekst"

#: ../clawsker:212
msgid "Text colour for tags in message view."
msgstr "Tekstfarge for markeringer i meldingsvindu"

#: ../clawsker:214
msgid "Default headers background"
msgstr "Standard backgrunn for brevhoder"

#: ../clawsker:215
msgid "Background colour for default headers in compose window."
msgstr "Bakgrunnsfarge for standard brevhoder i komponeringsvindu"

#: ../clawsker:216
msgid "Default headers text"
msgstr "Tekst for standard brevhoder"

#: ../clawsker:217
msgid "Text colour for default headers in compose window."
msgstr "Tekstfarge for standard brevhoder i komponeringsvindu"

#: ../clawsker:219
msgid "Active quick search background"
msgstr "Bakgrunn aktiv hurtigsøk"

#: ../clawsker:220
msgid "Background colour for active quick search."
msgstr "Bakgrunnsfarge for aktiv hurtigsøk"

#: ../clawsker:221
msgid "Active quick search text"
msgstr "Tekst aktiv hurtigsøk"

#: ../clawsker:222
msgid "Text colour for active quick search."
msgstr "Tekstfarge for aktiv hurtigsøk"

#: ../clawsker:223
msgid "Quick search error background"
msgstr "Bakgrunn hurtigsøk-feil"

#: ../clawsker:224
msgid "Background colour for quick search error."
msgstr "Bakgrunnsfarge for hurtigsøk-feil"

#: ../clawsker:225
msgid "Quick search error text"
msgstr "Tekstfarge hurtigsøkfeil"

#: ../clawsker:226
msgid "Text colour for quick search error."
msgstr "Tekstfarge for hurtigsøkfeil"

#: ../clawsker:228
msgid "Added lines"
msgstr "Tilleggslinjer"

#: ../clawsker:229
msgid "Colour for added lines in patches."
msgstr "Farge for tilleggslinjer i patcher."

#: ../clawsker:230
msgid "Deleted lines"
msgstr "Slettede linjer"

#: ../clawsker:231
msgid "Colour for deleted lines in patches."
msgstr "Farge for slettede linjer i patcher."

#: ../clawsker:232
msgid "Hunk lines"
msgstr "Rubrikk for stykke"

#: ../clawsker:233
msgid "Colour for hunk headers in patches."
msgstr "Farge for stykke-rubrikker i patcher."

#: ../clawsker:235
msgid "X position"
msgstr "X-posisjon"

#: ../clawsker:236
msgid "X coordinate for window's top-left corner."
msgstr "X-koordinat for vinduets øvre venstre hjørne."

#: ../clawsker:237
msgid "Y position"
msgstr "Y-posisjon"

#: ../clawsker:238
msgid "Y coordinate for window's top-left corner."
msgstr "Y-koordinat for vinduets øvre venstre hjørne."

#: ../clawsker:239
msgid "Width"
msgstr "Vidde"

#: ../clawsker:240
msgid "Window's width in pixels."
msgstr "Vinduets vidde i pixler."

#: ../clawsker:241
msgid "Height"
msgstr "Høyde"

#: ../clawsker:242
msgid "Window's height in pixels."
msgstr "Vinduets høyde i pixler."

#: ../clawsker:244
msgid "Maximized"
msgstr "Maksimert"

#: ../clawsker:245
msgid "Changes window maximized status."
msgstr "Endrer vinduets maksimerings-status."

#: ../clawsker:246
msgid "Full-screen"
msgstr "Fullskjerm"

#: ../clawsker:247
msgid "Changes full screen status."
msgstr "Endrer fullskjermsstatus."

#: ../clawsker:249
msgid "Use custom GnuTLS priority"
msgstr "Bruk valgfri GnuTLS prioritet"

#: ../clawsker:250
msgid "Enables using user provided GnuTLS priority string."
msgstr "Gjør det mulig for bruker å endre GnuTLS prioritets-streng."

#: ../clawsker:251 ../clawsker:2042
msgid "GnuTLS priority"
msgstr "GnuTLS-prioritet"

#: ../clawsker:252
msgid ""
"Value to use as GnuTLS priority string if custom priority check is enabled. "
"Otherwise this value is ignored."
msgstr ""
"Verdi som brukes som GnuTLS prioritets-streng hvis valgfri endring er på. "
"Ellers vil denne verdien ignoreres."

#: ../clawsker:254
msgid "Autocompletion limit"
msgstr "Grense for auto-komplettering"

#: ../clawsker:255
msgid ""
"Limits the number of addresses obtained from keyring through autocompletion. "
"Use 0 to get all matches."
msgstr ""
"Begrenser antall adresser som hentes fra nøkkelring gjennom auto-"
"komplettering. Bruk 0 for å få alle som matcher."

#: ../clawsker:256
msgid "Base URL"
msgstr "Base-URL"

#: ../clawsker:257
msgid ""
"This is the URL where avatar requests are sent. You can use the one of your "
"own libravatar server, if available."
msgstr ""
"Dette er URL avatar-forespørsler sendes til. Du kan bruke din egen "
"libravatar-server hvis tilgjengelig."

#: ../clawsker:258
msgid "Log level"
msgstr "Nivå på logg"

#: ../clawsker:259
msgid "Verbosity level of log, accumulative."
msgstr "Kumulativt verbositets-nivå på logg."

#: ../clawsker:260
msgid "None"
msgstr "Ingen"

#: ../clawsker:261
msgid "Manual"
msgstr "Manuell"

#: ../clawsker:262
msgid "Actions"
msgstr "Aktiviteter"

#: ../clawsker:263
msgid "Matches"
msgstr "Matcher"

#: ../clawsker:457
msgid "Clawsker error"
msgstr "Clawsker-feil"

#: ../clawsker:463
msgid "Error: seems Claws Mail is currently running, close it first."
msgstr "Feil: det virker som Claws Mail kjører nå, lukk den først."

#: ../clawsker:486
msgid "Error: resource file for Claws Mail was not found."
msgstr "Feil: ressursfilen for Claws Mail fantes ikke."

#: ../clawsker:1819
msgid "Main window"
msgstr "Hovedvindu"

#: ../clawsker:1832
msgid "Message window"
msgstr "Meldingsvindu"

#: ../clawsker:1842
msgid "Send window"
msgstr "Send-vindu"

#: ../clawsker:1847
msgid "Receive window"
msgstr "Mottaks-vindu"

#: ../clawsker:1861
msgid "Folder window"
msgstr "Mappe-vindu"

#: ../clawsker:1866
msgid "Folder selection window"
msgstr "Mappevalgs-vindu"

#: ../clawsker:1876
msgid "Addressbook main window"
msgstr "Hovedvindu for adressebok"

#: ../clawsker:1881
msgid "Edit person window"
msgstr "Editer person-vindu"

#: ../clawsker:1886
msgid "Edit group window"
msgstr "Editer gruppe-vindu"

#: ../clawsker:1891
msgid "Add address window"
msgstr "Legg til adresse-vindu"

#: ../clawsker:1896
msgid "Folder select window"
msgstr "Mappevalgs-vindu"

#: ../clawsker:1906
msgid "Accounts window"
msgstr "Konto-vindu"

#: ../clawsker:1911
msgid "Edit account window"
msgstr "Editer konto-vindu"

#: ../clawsker:1921
msgid "Filtering window"
msgstr "Filter-vindu"

#: ../clawsker:1926
msgid "Filtering actions window"
msgstr "Filter-handlinger vindu"

#: ../clawsker:1931
msgid "Filtering debug window"
msgstr "Debug-vindu for filter"

#: ../clawsker:1936
msgid "Matcher window"
msgstr "Match-vindu"

#: ../clawsker:1946
msgid "User Actions prefs window"
msgstr "Vindu for valg av Bruker-aktiviteter"

#: ../clawsker:1951
msgid "User Actions I/O window"
msgstr "Vindu for Bruker-aktiviteter I/O"

#: ../clawsker:1961
msgid "Preferences window"
msgstr "Vindu for Egenskaper"

#: ../clawsker:1966
msgid "Templates window"
msgstr "Vindu for Maler"

#: ../clawsker:1971
msgid "Tags window"
msgstr "Tags-vindu"

#: ../clawsker:1976
msgid "Plugins window"
msgstr "Plugins-vindu"

#: ../clawsker:1991
msgid "Print preview window"
msgstr "Forhåndsvisnings-vindu"

#: ../clawsker:1996
msgid "View source window"
msgstr "Vis kildekode"

#: ../clawsker:2003
msgid "Main"
msgstr "Hoved"

#: ../clawsker:2004
msgid "Message"
msgstr "Melding"

#: ../clawsker:2005
msgid "Send/Receive"
msgstr "Send/Motta"

#: ../clawsker:2006
msgid "Folder"
msgstr "Mappe"

#: ../clawsker:2009
msgid "Filtering"
msgstr "Filtrering"

#: ../clawsker:2010
msgid "User Actions"
msgstr "Bruker-aktiviteter"

#: ../clawsker:2011
msgid "Preferences"
msgstr "Innstillinger"

#: ../clawsker:2144
msgid "Attachment remover"
msgstr "Fjerning av vedlegg"

#: ../clawsker:2149
msgid "GPG"
msgstr "GPG"

#: ../clawsker:2155
msgid "Sieve manager"
msgstr "Sieve manager"

#: ../clawsker:2160
msgid "Libravatar"
msgstr "Libravatar"

#: ../clawsker:2165
msgid "Perl"
msgstr "Perl"

#: ../clawsker:2241
msgid "Menu path"
msgstr "Meny-sti"

#: ../clawsker:2254
msgid "Hotkey"
msgstr "Hurtigtast"

#: ../clawsker:2299
msgid "GLib runtime"
msgstr "GLib runtime"

#: ../clawsker:2300
msgid "GLib built"
msgstr "GLib bygget"

#: ../clawsker:2302
msgid "GTK2 runtime"
msgstr "GKT2 runtime"

#: ../clawsker:2303
msgid "GTK2 built"
msgstr "GTK2 bygget"

#: ../clawsker:2320
msgid "Binary"
msgstr "Binær"

#: ../clawsker:2321
msgid "Configuration"
msgstr "Konfigurasjon"

#: ../clawsker:2334
msgid "Library versions"
msgstr "Library-versjoner"

#: ../clawsker:2335
msgid "Claws Mail versions"
msgstr "Claws Mail versjoner"

#: ../clawsker:2362
#, perl-brace-format
msgid "Perl-GLib version {glibv}, built for {glibb}, running with {glibr}."
msgstr "Perl-GLib versjon {glibv}, bygget for {glibb}, kjører med {glibr}."

#: ../clawsker:2367
#, perl-brace-format
msgid "Perl-GLib version {glibv}."
msgstr "Perl-GLib versjon {glibv}."

#: ../clawsker:2371
#, perl-brace-format
msgid "Perl-GTK2 version {gtkv}, built for {gtkb}, running with {gtkr}."
msgstr "Perl-GTK2 versjon {gtkv}, bygget for {gtkb}, kjører med {gtkr}."

#: ../clawsker:2376
#, perl-brace-format
msgid "Perl-GTK2 version {gtkv}."
msgstr "Perl-GTK2 versjon {gtkv}."

#: ../clawsker:2380
msgid "Claws Mail was not found!"
msgstr "Claws Mail kunne ikke finnes!"

#: ../clawsker:2381
#, perl-brace-format
msgid "Claws Mail returned version {cmv}."
msgstr "Claws Mail returnerer versjon {cmv}."

#: ../clawsker:2392
msgid "Syntax:"
msgstr "Syntaks:"

#: ../clawsker:2393
msgid "  clawsker [options]"
msgstr "  clawsker [valg]"

#: ../clawsker:2394
msgid "Options:"
msgstr "Valg:"

#: ../clawsker:2395
msgid ""
"  -a|--alternate-config-dir <dir>  Uses <dir> as Claws Mail configuration."
msgstr ""
"  -a|--alternate-config-dir <dir>  Bruker <dir> som Claws Mail konfig-"
"katalog."

#: ../clawsker:2396
msgid "  -b|--verbose                     More messages on standard output."
msgstr "  -b|--verbose                     Fler meldinger til standard output."

#: ../clawsker:2397
msgid "  -c|--clawsrc <file>              Uses <file> as full resource name."
msgstr "  -c|--clawsrc <fil>              Bruker <fil> som fullt ressurs-navn."

#: ../clawsker:2398
msgid "  -h|--help                        Prints this help screen and exits."
msgstr "  -h|--help                        Viser denne hjelpe-skjermen."

#: ../clawsker:2399
msgid "  -r|--read-only                   Disables writing changes to disk."
msgstr ""
"  -r|--read-only                   Skrur av skriving av endringer til disk."

#: ../clawsker:2400
msgid ""
"  -v|--version                     Prints version information and exits."
msgstr "  -v|--version                     Viser versjons-info."

#: ../clawsker:2416
msgid "try -h or --help for syntax.\n"
msgstr "prøv -h eller --help for syntaks.\n"

#: ../clawsker:2419
#, perl-brace-format
msgid "Error in options: {msg}\n"
msgstr "Feil i valg: {msg}\n"

#: ../clawsker:2431
#, perl-brace-format
msgid "Error: {opt} requires a dotted numeric value argument\n"
msgstr "Feil: {opt} krever numerisk verdi som argument\n"

#: ../clawsker:2438
#, perl-brace-format
msgid "Error: '{dir}' is not a directory or does not exist\n"
msgstr "Feil: '{dir}' er ingen katalog eller finnes ikke\n"

#: ../clawsker:2447
#, perl-brace-format
msgid "Error: '{value}' is not a file or does not exist\n"
msgstr "Feil: '{value}' er ingen fil eller finnes ikke\n"

#: ../clawsker:2485 ../clawsker:2559
#, perl-brace-format
msgid "Error: opening '{file}' for reading"
msgstr "Feil: ved åpning av '{file}' for lesing"

#: ../clawsker:2493
#, perl-brace-format
msgid "Error: duplicate section '{sect}' in resource file '{file}'\n"
msgstr "Feil: duplisert seksjon '{sect}' i ressurs-fil '{file}'\n"

#: ../clawsker:2513 ../clawsker:2594
#, perl-brace-format
msgid "Error: opening '{file}' for writing"
msgstr "Feil: ved åpning av '{file}' for skriving"

#: ../clawsker:2547
#, perl-brace-format
msgid "Unable to create backup file '{name}'\n"
msgstr "Kunne ikke opprette backup-fil '{name}'\n"
