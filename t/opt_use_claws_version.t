use 5.010_000;
use strict;
use utf8;
use Test::More tests => 5;
use Test::Exception;

require_ok ('Clawsker');

use Clawsker;

ok ( defined &Clawsker::opt_use_claws_version, 'has function' );

is ( $Clawsker::CLAWSV, undef, 'not set' );

dies_ok { Clawsker::opt_use_claws_version('', 'version') } 'invalid';

Clawsker::opt_use_claws_version('', '1.2.3');

is ( $Clawsker::CLAWSV, '1.2.3', 'is set' );
