use 5.010_000;
use strict;
use utf8;
use Test::More tests => 7;
use Test::Exception;
use File::Spec::Functions;
use File::Temp qw(tempdir);

require_ok ('Clawsker');

my ($temp, $rc);

BEGIN {
  $temp = tempdir (CLEANUP => 1);
  $rc = catfile ($temp, 'alternaterc');
  open my $rcf, '>', $rc
    or die "opening $rc for writing: $!\n";
  close $rcf;
}

use Clawsker;

ok ( defined &Clawsker::opt_clawsrc, 'has function' );

is ( $Clawsker::CONFIGRC, 'clawsrc', 'init: rc' );
like ( $Clawsker::CONFIGDIR, qr{^.*\.claws-mail$}, 'init: dir' );

dies_ok { Clawsker::opt_clawsrc('', 'invalidrc') } 'invalid';

Clawsker::opt_clawsrc('', $rc);

is ( $Clawsker::CONFIGRC, 'alternaterc', 'init: rc' );
is ( $Clawsker::CONFIGDIR, "$temp/", 'init: dir' );
